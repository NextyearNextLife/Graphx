import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx.{Edge, Graph, VertexId, VertexRDD}
import org.apache.spark.rdd.RDD

object Join {
  def main(args: Array[String]): Unit = {
    //设置运行环境
    val conf = new SparkConf().setAppName("SimpleGraphX").setMaster("local")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

    val users: RDD[(VertexId, (String, Int))] =
      sc.parallelize(Array((1L, ("Alice", 28)),
        (2L, ("Bob", 27)),
        (3L, ("Charlie", 65)),
        (4L, ("David", 42)),
        (5L, ("Ed", 55)),
        (6L, ("Fran", 50))
      ))

    val relationships: RDD[Edge[Int]] =
      sc.parallelize(Array(
        Edge(2L, 1L, 7),
        Edge(2L, 4L, 2),
        Edge(3L, 2L, 4),
        Edge(3L, 6L, 3),
        Edge(4L, 1L, 1),
        Edge(5L, 2L, 2),
        Edge(5L, 3L, 8),
        Edge(5L, 6L, 3)
      )
      )

    // Build the initial Graph
    val graph = Graph(users, relationships)


    <!--OuterJoinVertices-->
    //graph有的就返回

    //通过获取图的顶点入度得到一个RDD
    var rdd: RDD[(VertexId, Boolean)] = sc.parallelize(Array(
      (2L, true),
      (3L, false),
      (4L, true),
      (5L, false)
    ))
    print(rdd)
    println("******************************************")
    graph.vertices.collect.foreach(println(_))

    //rddRcf  返回rdd的vd
    val graph2: Graph[(String, Int), Int] = graph.outerJoinVertices(rdd)((id, attr, rddRcf) => (
      rddRcf match {
        case Some(x) => if (x == true) (attr._1, attr._2 + 1) else (attr._1, attr._2 - 1)
        case None => (attr._1, 0)
      }
      ))

    println("******************************************")
    graph2.vertices.collect.foreach(println(_))


    <!--JoinVertices-->
    //graph和rdd2都有才返回
    var rdd2: RDD[(VertexId, Int)] = sc.parallelize(Array(
      (2L, 2),
      (3L, 3),
      (4L, 4),
      (5L, 5)
    ))


    var graph3: Graph[(String, Int), Int] = graph.joinVertices(rdd2)((id, attr, rddRcf) => (
      ((attr._1, attr._2 * (rddRcf + 1)))
      ))

    graph.vertices.collect.foreach(println(_))
    println("******************************************")
    graph3.vertices.collect.foreach(println(_))



  }
}
