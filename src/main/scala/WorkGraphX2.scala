import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx.{Edge, Graph, VertexId}
import org.apache.spark.rdd.RDD

object WorkGraphX2 {
  def main(args: Array[String]): Unit = {
    //设置运行环境
    val conf = new SparkConf().setAppName("SimpleGraphX").setMaster("local")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

    //设置users顶点
    val users: RDD[(VertexId, (String, Int))] =
      sc.parallelize(Array((1L, ("Alice", 28)),
        (2L, ("Bob", 27)),
        (3L, ("Charlie", 65)),
        (4L, ("David", 42)),
        (5L, ("Ed", 55)),
        (6L, ("Fran", 50))
      ))

    val relationships: RDD[Edge[Int]] =
      sc.parallelize(Array(
        Edge(2L, 1L, 7),
        Edge(2L, 4L, 2),
        Edge(3L, 2L, 4),
        Edge(3L, 6L, 3),
        Edge(4L, 1L, 1),
        Edge(5L, 2L, 2),
        Edge(5L, 3L, 8),
        Edge(5L, 6L, 3)
      )
      )
    // 定义默认的作者,以防与不存在的作者有relationship边
    val defaultUser = ("John Doe", 0)

    // Build the initial Graph
    var graph: Graph[(String, Int), Int] = Graph(users, relationships, defaultUser)

    //val newVertices = graph.vertices.map { case (id, attr) => (id, mapUdf(id, attr)) }
    //val newGraph = Graph(newVertices, graph.edges)

    graph.vertices.collect.foreach(println(_))

    //    ---------------MapVertices------------
    // vd => vd
    var graph2: Graph[(String, Int), Int] = graph.mapVertices((vid: VertexId, attr: (String, Int)) => (attr._1, 2 * attr._2))
    println("-----------MapVertices--------------")
    graph2.vertices.collect.foreach(println(_))



    //-------------MapEdges-------------
    //  ed => ed2
    // 对象
    //
    var graph3: Graph[(String, Int), (String, String)] = graph.mapEdges(e => (e.attr + "ee", "mak"))
    println("-----------MapEdges--------------")
    graph3.edges.collect.foreach(println(_))


    //---------------MapTripets--------------
    //   vd ed => ed2
    //对象
    var graph4: Graph[(String, Int), (String, String)] = graph.mapTriplets(Triple => (Triple.attr + "e3", "mak"))
    println("-----------MapTripets--------------")
    graph4.edges.collect.foreach(println(_))
    graph4.vertices.collect.foreach(println(_))


  }
}
