import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx.{Edge, Graph, VertexId, VertexRDD}
import org.apache.spark.rdd.RDD

object Aggregate {
  def main(args: Array[String]): Unit = {

    val conf = new SparkConf().setAppName("SimpleGraphX").setMaster("local")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")


    val vertex: RDD[(VertexId, Int)] = sc.parallelize(Array((1L, 28),
      (2L, 18),
      (3L, 16),
      (4L, 25),
      (5L, 33),
      (6L, 17)
    ))

    val edge: RDD[Edge[Int]] =
      sc.parallelize(Array(
        Edge(2L, 1L, 7),
        Edge(2L, 1L, 2),
        Edge(3L, 2L, 4),
        Edge(3L, 6L, 3),
        Edge(4L, 1L, 1),
        Edge(5L, 2L, 2),
        Edge(5L, 3L, 8),
        Edge(5L, 6L, 3)
      )
      )

    val graph = Graph(vertex, edge)

    val yangFollower: VertexRDD[(Int, Double)] = graph.aggregateMessages[(Int, Double)](triplet => {
      if (triplet.srcAttr < triplet.dstAttr) {
        triplet.sendToDst(1, triplet.srcAttr)
      }
    },
      (a, b) => (a._1 + b._1, a._2 + b._2)
    )

    val avgyangFollower: VertexRDD[Double] = yangFollower.mapValues((id, value) =>
      value match {
        case (count, totalAge) => totalAge / count
      }
    )

    println("--------------------------------------")
    yangFollower.collect.foreach(println(_))
    println("--------------------------------------")
    avgyangFollower.collect.foreach(println(_))



  }

}
