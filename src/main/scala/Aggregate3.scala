import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx.{Edge, EdgeContext, Graph, VertexRDD}

object Aggregate3 {
  def sendMsg(ec:EdgeContext[Int,String,Int]):Unit = {
      ec.sendToDst( ec.srcAttr +1)
  }

  def mergeMsg(a: Int , b:Int) :Int = {
    math.max(a,b)
  }





  def sumEdgeCount( g:Graph[Int,String],x:Int):Graph[Int,String] = {
    var verts: VertexRDD[Int] = g.aggregateMessages[Int](sendMsg, mergeMsg)
    var num: Int = verts.collect.map(x => x._2).reduce(_ + _)
    println("num:  -----" + num)

    val g2 = Graph(verts ,g.edges)

    //(4,(1,0))
    println( "G2  *************************************")
    g2.vertices.collect.foreach(println(_))
    //g2.vertices.join(g.vertices).map( x => x._2._1 - x._2._2).reduce(_+_).collect.foreach(println(_))
//    val check = g2.vertices.join(g.vertices).map( x => x._2._1 - x._2._2).reduce(_+_)
    println( "*************************************")

    if  (num != x){
      sumEdgeCount(g2,num)
    } else{
      g

    }

  }



  def main(args: Array[String]): Unit = {

    //设置运行环境
    val conf = new SparkConf().setAppName("SimpleGraphX").setMaster("local")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

    // 构建图
    val myVertices = sc.parallelize(Array((1L, "张三"), (2L, "李四"), (3L, "王五"), (4L, "钱六"),
      (5L, "领导")))
    val myEdges = sc.makeRDD(Array( Edge(1L,2L,"朋友"),
      Edge(2L,3L,"朋友") , Edge(3L,4L,"朋友"),
      Edge(4L,5L,"上下级"),Edge(3L,5L,"上下级")
    ))

    val myGraph = Graph(myVertices,myEdges)

    var initGraph: Graph[Int, String] = myGraph.mapVertices((_, _) => 0)


    sumEdgeCount(initGraph,initGraph.vertices.map(x=>x._2).reduce(_+_)).vertices.collect.foreach(println(_))



  }


}
