import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx.{Edge, Graph, VertexId}
import org.apache.spark.rdd.RDD

object WorkGraphX3 {

  def main(args: Array[String]): Unit = {
    //设置运行环境
    val conf = new SparkConf().setAppName("SimpleGraphX").setMaster("local")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

    //设置users顶点
    val users: RDD[(VertexId, (String, Int))] =
      sc.parallelize(Array((1L, ("Alice", 28)),
        (2L, ("Bob", 27)),
        (3L, ("Charlie", 65)),
        (4L, ("David", 42)),
        (5L, ("Ed", 55)),
        (6L, ("Fran", 50))
      ))

    val relationships: RDD[Edge[Int]] =
      sc.parallelize(Array(
        Edge(2L, 1L, 7),
        Edge(2L, 1L, 2),
        Edge(3L, 2L, 4),
        Edge(3L, 6L, 3),
        Edge(4L, 1L, 1),
        Edge(5L, 2L, 2),
        Edge(5L, 30L, 8),
        Edge(5L, 6L, 3)
      )
      )
    // 定义默认的作者,以防与不存在的作者有relationship边
    val defaultUser = ("John Doe", 0)

    // Build the initial Graph
    var graph: Graph[(String, Int), Int] = Graph(users, relationships, defaultUser)
    graph.edges.collect.foreach(println(_))
    graph.vertices.collect.foreach(println(_))
    println("--------------------------------------------------------------")


   <!--MASK--->

    var graph2: Graph[(String, Int), Int] = graph.subgraph(ep => (ep.dstId != 30L && ep.dstAttr._2 != 0))
    graph2.edges.collect.foreach(println(_))
    graph2.vertices.collect.foreach(println(_))
    println("--------------------------------------------------------------")

    var graph3: Graph[(String, Int), Int] = graph.mask(graph2)
    graph3.edges.collect.foreach(println(_))
    graph3.vertices.collect.foreach(println(_))


    <!--GroupEdges-->
    //操作相同ed
    var graph4: Graph[(String, Int), Int] = graph.groupEdges(merge = (ed1, ed2) => (ed1 + ed2))
    graph4.edges.collect.foreach(println(_))




    <!--Reverses-->
    //反向图
    var graph5: Graph[(String, Int), Int] = graph.reverse
    graph5.edges.collect.foreach(println(_))



  }

}
